<!DOCTYPE html>
<html>
<head>
  <title>Training</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="./assets/css/style.css">

</head>
<body>
  <header>
    <div class="container flex flexJustifyBetween flex
    AlignItemsCenter">
      <a href="index.html">
        <div id="logo">
          Logo
        </div>
      </a>
      <nav class="flex">
        <a href="sign-in.html">
          <div class="btn">
            Sign In
          </div>
        </a>
        <div class="pad5"></div>
        <a href="sign-up.html">
          <div class="btn">
            Sign Up
          </div>
        </a>
      </nav>
    </div>
  </header>
  <main class="container">
    <section id="productsContainer" class="flex flexWrap">

      <article class="item bdr">
        <a href="product.html?id=1">
          <img src="./assets/images/1.jpg" class="imgResponsive">
          <div>Product 1</div>
        </a>
      </article>

      <article class="item bdr">
        <a href="product.html?id=2">
          <img src="./assets/images/2.jpg" class="imgResponsive">
          <div>Product 2</div>
        </a>
      </article>
      <article class="item bdr">
        <a href="product.html?id=3">
          <img src="./assets/images/3.jpg" class="imgResponsive">
          <div>Product 3</div>
        </a>
      </article>
      <article class="item bdr">
        <a href="product.html?id=4">
          <img src="./assets/images/4.jpg" class="imgResponsive">
          <div>Product 4</div>
        </a>
      </article>
    </section>
  </main>
  <footer>

  </footer>
</body>
</html>
